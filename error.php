<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>お問い合わせフォーム</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container">
      <h2>お問い合わせフォーム</h2>
      <!--
      <p class="bg-danger">入力エラーがあります。</p>
      -->
      <div class="alert alert-danger" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <span class="sr-only">Error:</span>
        入力エラーがあります。
      </div>

      <form>
        <div class="form-group has-error has-feedback">
          <label for="exampleInputName">お名前</label>
          <input type="text" class="form-control" id="exampleInputPassword1" placeholder="name">
          <span  class="help-block">必須入力です。</span>
        </div>
        <div class="form-group has-error has-feedback">
          <label for="exampleInputEmail1">メールアドレス</label>
          <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
          <span  class="help-block">メールの形式が不正です。</span>
        </div>
        <div class="form-group has-error has-feedback">
          <label for="exampleInputEmail1">内容</label>
          <textarea class="form-control"></textarea>
          <span  class="help-block">1000文字以内です。</span>
        </div>

        <!--
        <div class="checkbox">
          <label>
            <input type="checkbox"> Check me out
          </label>
        </div>
        -->

        <button type="submit" class="btn btn-default">送信する</button>
      </form>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
