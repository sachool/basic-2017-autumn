<?php
if($_SERVER["REQUEST_METHOD"] == "POST"){
  //POST値の受け取り
  $email = $_POST['email'];
  $name  = $_POST['name'];
  $body  = $_POST['body'];
  unset($errors);

   //名前
   if (!isset($name) || $name === '') {
       $errors['name'] = '名前が入力されていません';
   } elseif (strlen($name)>100) {
       $errors['name'] = '100文字までです';
   }

  //Eメールアドレス
   if(!isset($email) || $email === ''){
       $errors['email'] = 'Eメールアドレスが入力されていません';
   }
   elseif(strlen($email)>=100){
       $errors['email'] = 'Eメールアドレスは100文字以内で入力してください';
   }
   else{
       if (filter_var($email, FILTER_VALIDATE_EMAIL) !== false) {
         //echo '正しいEメールアドレス形式です';
       }
       else{
         $errors['email'] = 'Eメールアドレスの形式が不正です';
       }
   }
  //内容
  if (!isset($body) || $body === '') {
     $errors['body'] = '内容が入力されていません';
  } elseif (strlen($body)>1000) {
     $errors['body'] = '1000文字までです';
  }

    //errorが無ければ
    //完了画面に遷移
    if(!isset($errors)){
      header('Location: complete.php');
      exit(1);
    }
}
?>
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>お問い合わせフォーム</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container">
      <h2>お問い合わせフォーム</h2>

      <?php if (isset($errors)): ?>
        <div class="alert alert-danger" role="alert">
          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
          <span class="sr-only">Error:</span>
          入力エラーがあります。
        </div>
      <?php endif; ?>

      <form action="./" method="post">
        <div class="form-group <?php if (isset($errors['name'])): ?>has-error has-feedback<?php endif; ?>">
          <label for="exampleInputName">お名前</label>
          <input type="text" name="name" class="form-control" id="" placeholder="name">
          <?php if (isset($errors['name'])): ?>
            <span class="help-block"><?=$errors['name'];?></span>
          <?php endif; ?>
        </div>
        <div class="form-group <?php if (isset($errors['email'])): ?>has-error has-feedback<?php endif; ?>">
          <label for="exampleInputEmail1">メールアドレス</label>
          <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
          <?php if (isset($errors['email'])): ?>
            <span class="help-block"><?=$errors['email'];?></span>
          <?php endif; ?>
        </div>
        <div class="form-group <?php if (isset($errors['body'])): ?>has-error has-feedback<?php endif; ?>">
          <label for="exampleInputBody">内容</label>
          <textarea name="body" class="form-control"></textarea>
          <?php if (isset($errors['body'])): ?>
            <span class="help-block"><?=$errors['body'];?></span>
          <?php endif; ?>
        </div>

        <!--
        <div class="checkbox">
          <label>
            <input type="checkbox"> Check me out
          </label>
        </div>
        -->

        <button type="submit" class="btn btn-default">送信する</button>
      </form>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
